<!DOCTYPE html>
<html>

<head>

    <title>Prezentační stránka</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo URL ?>public/css/default.css" />

    <script type="text/javascript" src="<?php echo URL ?>public/js/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</head>

<body>


    <div id="header">

        <div class="topnav">
            <img src="public/images/expanding.png" alt="logo" id="logo" />


            <script>
            var isExpanded = true;
            document.getElementById("logo").onclick = function() {
                change()
            };

            /**
             * Zmeni ikonu #logo dle potreby. Pokud je menu rozklikle -> zmeni na ikonu rozklikleho 
             * a naopak. 
             */
            function change() {
                if (isExpanded == true) {
                    document.getElementById('logo').src = "public/images/noExpand.png";


                    $("li").animate({
                        height: 'toggle'
                    });

                    isExpanded = false;
                } else {
                    document.getElementById('logo').src = "public/images/expanding.png";
                    $("li").animate({
                        height: 'toggle'
                    });
                    isExpanded = true;
                }
            }
            </script>

            <br />
            <nav>
                <ul class="nav-ul" id="nav-ul">
                    <li><a class="ahref" href="<?php echo URL ?>index"><strong>Úvod</strong></a></li>
                    <li><a class="ahref" href="<?php echo URL ?>vzdelani"><strong>Vzdělání</strong></a></li>
                    <li><a class="ahref" href="<?php echo URL ?>kontakt"><strong>Kontakt</strong></a></li>
                </ul>
            </nav>
        </div>
    </div>

    <div id="content">