<div id="experiences_Page">
    <div class="content">


        <div class="section-container">


            <div class="text">
                <h3>2012-2016</h3>
            </div>
            <img class="images" src="public/images/velesin.jpg"
                alt="Střední odborná škola technická a elektrotechnická Velešín">

            <div class="info">
                <h3>Střední odborná škola strojní a elektrotechnická Velešín</h3>
                <h4>Maturitní obor Elektrotechnika - Počítačové systémy </h4>
                <p>Střední škola mi dala, krom maturity, vcelku široký úvod do světa IT.</br></p>
                <p>Začalo to Scratchem a pokračovalo to dalšími jazyky (Java, SQL, CSS, HTML).</a></p>
            </div>


        </div>


        <div class="section-container">



            <div class="text">
                <h3>2016-2018, 2019-2020</h3>
            </div>
            <img class="images" src="public/images/fav.jpg" alt="Fakulta aplikovaných věd na Západočeské univerzitě">
            <div class="info">
                <h3>Fakulta aplikovaných věd na Západočeské univerzitě</h3>
                <h4>Bakalářský obor Informatika a výpočetní technika </h4>
                <p>Na Západočeské univerzitě jsem "přičichl", či se ještě více zdokonalil ve širokém spektru odvětví,
                    technik či postupů. </p>
                    <p>A to od návrhu programů, přes implementaci až po samotné testování.</p>
                    <p>Studium zanecháno.</p>
                <p></p>
            </div>

        </div>

    </div>

</div>