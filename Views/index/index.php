<div class="content">




    <section class="aboutMe">
        <h2>O mě</h2>
        <p>Vítejte, jmenuji se Vlastimil Prukner </br></p>
        <p>toto jsou mé prezentační stránky na pozici <strong>Front-End Kodér – Junior</strong>.</p>

        
    </section>
    <h1>Schopnosti</h1>
    </br>
    <section id="section-abilities">


        <div class="ability advanced">
            <h4>Java</h4>
            <img src="public/images/icons/java.webp" alt="logo" class="logo" />
            <p class="hidden">Podrobná znalost. Letitá zkušenost jak ze školních projektů,
          tak i z vývoje aplikací k osobnímu užití.</p>
            <p></p>
        </div>

        <div class="ability advanced">
            <h4>HTML</h4>
            <img src="public/images/icons/html.png" alt="logo" class="logo" />
            <p class="hidden">To je samozřejmost ne?</br> </p>

            <p></p>
        </div>
        <div class="ability advanced">
            <h4>CSS</h4>
            <img src="public/images/icons/css.png" alt="logo" class="logo" />
            <p class="hidden">Rozsáhlý přehled. Znalost preprocesoru Sass. </br> </p>
            <p class="hidden">Responzivní design. </p>
            <p></p>
        </div>
        <div class="ability advanced">
            <h4>JavaScript</h4>
            <img src="public/images/icons/javascript.png" alt="logo" class="logo" />
            <p class="hidden">Znalost syntaxe. Povědomí o knihovně jQuery.</br></p>
            <p class="hidden"> Zkušenost již ze střední školy.</br></p>
            <p></p>
        </div>
        <div class="ability">
            <h4>PHP</h4>
            <img src="public/images/icons/php.png" alt="logo" class="logo" />
            <p class="hidden">Základní jazykové konstrukce. </br> </p>
         
            <p></p>
        </div>
        <div class="ability">
            <h4>Python</h4>
            <img src="public/images/icons/python.webp" alt="logo" class="logo" />
            <p class="hidden">Základní přehled co se týče syntaxe. </br> </p>
            <p class="hidden">Povědomí o možnostech jazyka.</p>
            <p></p>
        </div>
        <div class="ability advanced">
            <h4>SQL</h4>
            <img src="public/images/icons/sql.png" alt="logo" class="logo" />
            <p class="hidden">Selekty všeho druhu. Normalizace mi také není cizí.</br> </p>
            <p class="hidden">Vytvořeno několik projektů školního rozsahu.</p>
            <p></p>
        </div>
        <div class="ability">
            <h4>Selenium</h4>
            <img src="public/images/icons/selenium.png" alt="logo" class="logo" />
            <p class="hidden">Zkušenost s testovacím frameworkem Selenium, konkrétně WebDriver.</br> </p>
            <p class="hidden">Znalost testovacích metod a postupů v rozsahu CaSTB.</p>
            <p></p>
        </div>
        <div class="ability">
            <h4>Multimédia</h4>
            <img src="public/images/icons/photoshop.png" alt="logo" class="logo" />
            <p class="hidden">Přehled v editorech grafiky - např. Adobe Photoshop, Gimp, Figma...</br></p>
           <p class="hidden"> </p>
            <p></p>
        </div>
        <div class="ability advanced">

            <h4>Schopnosti</h4>
            <img src="public/images/icons/zarovka.png" alt="logo" class="logo" />
            <p class="hidden">Moje největší devíza - pracovitost</br> a ochota učit se novým věcem.</br> </p>
            <p></p>
        </div>
    </section>

    <section class="legend">
        <p class="advanced">pokročilý</p>
        <p class="ability">základní přehled</p>
    </section>



</div>