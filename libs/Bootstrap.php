<?php

class Bootstrap
{
    /**
     * Upravi (oseka) url na vzhlednejsi adresu
     */
    public function __construct()
    {
        $url = isset($_GET['url'])?$_GET['url']:'index';

        $url = rtrim($url, '/');
        $url = explode('/', $url);
        
      
        
        if(empty($url[0])){
            require 'Controllers/index.php';
            $controller = new Index();
            $controller->index();
            return false;
        }
        $file= 'Controllers/' . $url[0] . '.php';
        if(file_exists($file)){
            require $file;
        }else{
            require 'Controllers/err.php';
           $controller = new Err();
           return false;
        }
        $controller = new $url[0];
        
        //
        if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            }else{
                echo 'chyba';
            }
      
        } else {
            if (isset($url[1])) {
                $controller->{$url[1]}();
              
            }else{
                $controller->index();
            }
        }

     
    }
}
